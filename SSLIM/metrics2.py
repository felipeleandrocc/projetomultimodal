from decimal import Decimal

PRECISION_AT = 20

def compute_precision(recommendations, test_file,user_sideinformation_file):
    """
    Computes recommendation precision based on a tsv test dog.
    """
    ## Computing precision
    # Organizing data
    import os
    user_item = {}
    with open(test_file) as test_file:
        for line in test_file:
            u, i, v = line.strip().split(' ')
            u, i = int(u), int(i)
            # TODO: accept float =/
            v = 1
            if u in user_item:
                user_item[u].add(i)
            else:
                user_item[u] = set([i])

    precisions = []
    # Computing
    total_users = Decimal(len(recommendations.keys()))
    end = '/home/felipe/Doutorado/Backup/Projeto/toyslim-master/Result_SSLIM/' + user_sideinformation_file + '/'
    os.mkdir (end)
    file2 = end  + '/Precisao.txt'
    f3 = open(file2, 'w')
    f4 = open(end  + '/Revocacao.txt', 'w')
    for at in range(1, PRECISION_AT+1):

        file_prec = open(end + 'Precisao_' + str(at), 'w')
        mean = 0
        revocacao = 0
        for u in recommendations.keys():

            relevants = user_item[u]
            retrieved = recommendations[u][:at]
            precision = len(relevants & set(retrieved))/Decimal(len(retrieved))
            rev= len(relevants & set(retrieved))/Decimal(len(relevants))
            revocacao += rev
            mean += precision
            file_prec.write(str(relevants & set(retrieved)) + ' ' + str(set(retrieved)) + '  '+ str(precision) + '\n' )


        #print mean , total_users
        print 'Precisao Media @%s: %f   | Revocacao Media @%s: %f ' % (at, (mean/total_users), at, (revocacao/total_users) )
        f3.write(str(at) + ': ' + str(mean/total_users) + '\n')
        f4.write(str(at) + ': ' + str(revocacao/total_users) + '\n')

        precisions.append([at, (mean/total_users)])

    return precisions



def compute_precision_as_an_oracle(recommendations, test_file):
    """
    Computes recommendation precision based on a tsv test dog but computing it
    as we had an oracle that predicts the best ranking that can be done with
    the recommendations.
    """
    ## Computing precision
    # Organizing data
    user_item = {}
    with open(test_file) as test_file:
        for line in test_file:
            u, i, v = line.strip().split(' ')
            u, i = int(u), int(i)
            # TODO: accept float =/
            v = 1
            if u in user_item:
                user_item[u].add(i)
            else:
                user_item[u] = set([i])

    total_users = Decimal(len(recommendations.keys()))

    # Changing recommendations as an ORACLE
    for u in recommendations.keys():
        recommendations[u] = list(user_item[u] & set(recommendations[u]))
        recommendations[u] += list(
            user_item[u] | set(recommendations[u]) -
            (user_item[u] & set(recommendations[u]))
        )

    # Computing
    for at in range(1, PRECISION_AT+1):
        mean = 0
        for u in recommendations.keys():
            relevants = user_item[u]
            retrieved = recommendations[u][:at]
            precision = len(relevants & set(retrieved))/Decimal(len(retrieved))
            mean += precision

        print 'Average Precision @%s: %s' % (at, (mean/total_users))

