# -*- coding: UTF-8 -*-
from decimal import Decimal
from time import sleep

PRECISION_AT = 20

def compute_precision(recommendations, test_file,user_sideinformation_file,beta):
    """
    Computes recommendation precision based on a tsv test dog.
    """
    ## Computing precision
    # Organizing data
    import os
    user_item = {}
    with open(test_file) as test_file:
        for line in test_file:
            u, i, v = line.strip().split(' ')
            u, i = int(u), int(i)
            # TODO: accept float =/
            v = 1
            if u in user_item:
                user_item[u].add(i)
            else:
                user_item[u] = set([i])

    precisions = []
    # Computing
    print('Rec: ',recommendations)
    total_users = Decimal(len(recommendations.keys()))
    end = 'SideInformation/Result_SSLIM/' + user_sideinformation_file + 'Beta_' + str(beta) + '/'
    if (os.path.exists(end)):
      import shutil
      shutil.rmtree(end)
    os.mkdir(end)
    file2 = end  + '/Precisao.txt'
    f3 = open(file2, 'w')
    f4 = open(end  + '/Revocacao.txt', 'w')
    f5 = open(end  + '/F1.txt', 'w')
    f6 = open(end  + '/Percentual_Acerto_Ranking.txt', 'w')
    f7 = open(end  + '/DesvioPadrao.txt', 'w')
    f8 = open(end  + '/CoeficientedeVariacao.txt', 'w')
    f9 = open(end  + '/MargemErro.txt', 'w')
    for at in range(1, PRECISION_AT+1):

        file_prec = open(end + 'Precisao_' + str(at), 'w')
        file_prec2 = open(end + 'RELEVANTES_&_RECOMENDADO_' + str(at), 'w')

        mean = 0
        revocacao = 0
        f1 = 0
        Percentual_Acerto = 0
        Desvio = 0
        prec = 0
        precision = 0

        for u in recommendations.keys():


            #sleep (2)
            try:
              relevants = user_item[u]
            except:
              pass

            retrieved = recommendations[u][:at]
            precision = len(relevants & set(retrieved))/Decimal(len(retrieved))

            rev= len(relevants & set(retrieved))/Decimal(len(relevants))
            revocacao += rev
            mean += precision
            if precision == 1:
              Percentual_Acerto = Percentual_Acerto + 1
            file_prec.write(str(relevants & set(retrieved)) + ' ' + str(set(retrieved)) + '  '+ str(precision) + '\n' )
            file_prec2.write(str(u) + '|||' + str(relevants) + '|||' + str(set(retrieved)) + '|||' + str(precision) + '\n')

        #print mean , total_users


        prec = mean/total_users
        #####################Calcular o Desvio Padrão########

        import math

        for XX in recommendations.keys():
            try:
             relevants = user_item[XX]
            except:
             pass
            retrieved = recommendations[XX][:at]
            precision = len(relevants & set(retrieved))/Decimal(len(retrieved))
            de = ( (precision - prec) * (precision - prec) )
            Desvio = Desvio + de

        Desvio = math.sqrt(  (1/(total_users - 1) ) * ( Desvio ) )

        ###################################################
        revoc = revocacao/total_users
        if (prec + revoc) > 0:
          f1 = 2 * ( (prec * revoc) / (prec + revoc) )
        else:
          f1 = 0

        if prec > 0:
          cv = float(Desvio) / float(prec)
        else:
          cv = 0
        ##Margem de 90%
        MargemErro = 1.65 * cv

        #print 'Precisao Media @%s: %f   | Revocacao Media @%s: %f | F1 Media @%s: %f | Percentual User Acerto @%s: %f | Desvio @%s: %f  | CV @%s: %f | Erro @%s: %f  ' % (at, (mean/total_users), at, (revocacao/total_users), at, (f1) , at, (Percentual_Acerto / total_users), at, (Desvio), at, (cv) , at, (MargemErro))
        print ('Precisao Media @%s: %f ' % (at, (mean/total_users) ))
        f3.write(str(at) + ': ' + str(mean/total_users) + '\n')
        f4.write(str(at) + ': ' + str(revocacao/total_users) + '\n')
        f5.write(str(at) + ': ' + str(f1) + '\n')
        f6.write(str(at) + ': ' + str((Percentual_Acerto / total_users) ) + '\n')
        f7.write(str(at) + ': ' + str(Desvio) + '\n')
        f8.write(str(at) + ': ' + str(  cv ) + '\n')
        f9.write(str(at) + ': ' + str(  MargemErro ) + '\n')
        precisions.append([at, (mean/total_users)])


    return precisions



def compute_precision_as_an_oracle(recommendations, test_file):
    """
    Computes recommendation precision based on a tsv test dog but computing it
    as we had an oracle that predicts the best ranking that can be done with
    the recommendations.
    """
    ## Computing precision
    # Organizing data
    user_item = {}
    with open(test_file) as test_file:
        for line in test_file:
            u, i, v = line.strip().split(' ')
            u, i = int(u), int(i)
            # TODO: accept float =/
            v = 1
            if u in user_item:
                user_item[u].add(i)
            else:
                user_item[u] = set([i])

    total_users = Decimal(len(recommendations.keys()))

    # Changing recommendations as an ORACLE
    for u in recommendations.keys():
        recommendations[u] = list(user_item[u] & set(recommendations[u]))
        recommendations[u] += list(
            user_item[u] | set(recommendations[u]) -
            (user_item[u] & set(recommendations[u]))
        )

    # Computing
    for at in range(1, PRECISION_AT+1):
        mean = 0
        for u in recommendations.keys():
            relevants = user_item[u]
            retrieved = recommendations[u][:at]
            precision = len(relevants & set(retrieved))/Decimal(len(retrieved))
            mean += precision

        print ('Average Precision @%s: %s' % (at, (mean/total_users)))




