#!/usr/local/bin/python
# -*- coding: UTF-8 -*-
"""
cSLIM Parallel implementation. To understand deeply how it works we encourage you to
read "Sparse Linear Methods with Side Information for Top-N Recommendations"

Movielens 1M
python2 sslim_parallel.py --train=treino.tsv --side_information=sideInformation_aux.tsv --test=teste.csv --beta=1 --normalize=1 --output=saidaTexto.out


Movielens 100k
python sslim_parallel.py --train=userItem_treino.tsv --side_information=sideInformation100k.tsv --test=userItem_teste.tsv --beta=5 --normalize=1 --output=saidaTexto.out
python sslim_parallel.py --train=userItem_treino.tsv --side_information=sideInformation100k_binario.tsv --test=userItem_teste.tsv --beta=5 --normalize=1 --output=saidaTexto.out




python sslim_parallel.py --train=userItem.tsv --side_information=sideInformation1M.tsv --test=userItem.tsv --beta=1 --normalize=1 --output=saidaTexto.out

python sslim_parallel.py --train=userItem_treino.tsv --side_information=sideInformation1M_Binario_VersaoXmaiores.tsv --test=userItem_teste.tsv --beta=1 --normalize=1 --output=saidaTexto.out
python sslim_parallel.py --train=userItem_treino.tsv --side_information=sideInformation1M_Binario_VersaoXmaiores_0.2.tsv --test=userItem_teste.tsv --beta=1 --normalize=1 --output=saidaTexto.out
python sslim_parallel.py --train=userItem_treino.tsv --side_information=sideInformation1M_Binario_VersaoXmaiores_0.5.tsv --test=userItem_teste.tsv --beta=1 --normalize=1 --output=saidaTexto.out




"""

import ctypes
import datetime

import numpy as np
from multiprocessing import Pool
import multiprocessing
import multiprocessing as mp
from metrics import compute_precision
from metrics import compute_precision_as_an_oracle
from scipy.sparse import vstack
from sklearn.linear_model import SGDRegressor

from recommender import slim_recommender
from util import (tsv_to_matrix, generate_slices,
                  make_compatible, normalize_values, save_matrix)

import multiprocessing as mp

beta = 1






print ('>>> Start: %s' % datetime.datetime.now())


def work(params,W):
    from_j = params[0]
    #from_j = from_j[0]

    to_j = params[1]
    #to_j = to_j[1]

    M = params[2]
    #M = M[2]
    model = params[3]
    #model = model[3]
    counter = 0
    #print(params[0])
    #print(params[1])

    #print(range(from_j, to_j))
    #print(M.shape)
    #print(len(params))

    for j in range(from_j, to_j):
        counter += 1
        if counter % 10 == 0:
            #print 'Range  -> %s: %maria.2f%%',(from_j, to_j)
            print(counter/float(to_j - from_j)) * 100
            dada = 1
        mlinej = M[:, j].copy()

        # We need to remove the column j before training
        M[:, j] = 0

        model.fit(M, mlinej.toarray().ravel())

        # We need to reinstate the matrix
        M[:, j] = mlinej

        w = model.coef_

        # Removing negative values because it makes no sense in our approach
        w[w < 0] = 0

        for el in w.nonzero()[0]:
            W[(el, j)] = w[el]


if __name__ == '__main__':
    mp.freeze_support()
    train_file = 'userItem_treino.tsv'
    user_sideinformation_file = 'sideInformation100k_binariobeta5.0_fold0.Wmatrix.tsv'
    print ('**************************************************************************************************************')
    print (user_sideinformation_file)
    print ('\n\n\n\n')
    output = 'saida.out'
    test_file = 'userItem_teste.tsv'
    beta = 1
    normalize = 1
    fold = 0
    # Loading matrices
    A = tsv_to_matrix(train_file)
    print('felipe MATRIX A')
    B = tsv_to_matrix(user_sideinformation_file)
    print('felipe MATRIX B')
    if normalize:
         B = normalize_values(B)
    print('Tamanho B: ', B.shape)
    A, B = make_compatible(A, B)
    print('felipe COMPATIVEL AB')
    # Loading shared array to be used in results
    shared_array_base = multiprocessing.Array(ctypes.c_double, A.shape[1]**2)
    shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
    shared_array = shared_array.reshape(A.shape[1], A.shape[1])

    print('ARRAY')
    # We create a work function to fit each one of the columns of our W matrix,
    # because in SLIM each column is independent we can use make this work in
    # parallel



    def sslim_train(A, B, l1_reg=0.001, l2_reg=0.0001, beta=beta):
        """
        Computes W matrix of SLIM

        This link is useful to understand the parameters used:

            http://web.stanford.edu/~hastie/glmnet_matlab/intro.html

            Basically, we are using this:

                Sum( yi - B0 - xTB) + ...
            As:
                Sum( aj - 0 - ATwj) + ...

        Remember that we are wanting to learn wj. If you don't undestand this
        mathematical notation, I suggest you to read section III of:

            http://glaros.dtc.umn.edu/gkhome/slim/overview
        """
        alpha = l1_reg + l2_reg
        # alpha = 0.001
        l1_ratio = l1_reg / alpha

        model = SGDRegressor(
            penalty='elasticnet',
            fit_intercept=False,
            alpha=alpha,
            l1_ratio=l1_ratio
        )

        # Following cSLIM proposal on creating an M' matrix = [ M, FT ]
        # * beta is used to control relative importance of the side information
        Balpha =  B
        Mline = vstack((A, Balpha), format='lil')

        # Fit each column of W separately. We put something in each positions of W
        # to allow us direct indexing of each position in parallel
        total_columns = Mline.shape[1]
        ranges = generate_slices(total_columns)
        separated_tasks = []

        for from_j, to_j in ranges:
            separated_tasks.append([from_j, to_j, Mline, model])

        aux = separated_tasks[0]

        print(separated_tasks[0])

        processes = []
        for i in range(0, 8):
            p = mp.Process(target=work, args=(separated_tasks[i],shared_array))
            processes.append(p)
            p.start()
        for process in processes:
            process.join()
        #pool = multiprocessing.Pool()
        #pool.map(work, separated_tasks)
        #pool.close()
        #pool.join()

        return shared_array
    print('felipe')
    W = sslim_train(A, B, beta=beta)
    print('Passou aqui7')
    save_matrix(W, user_sideinformation_file.replace('.tsv', 'beta%s_fold%s.Wmatrix.tsv' % (beta, fold)))
    print('Passou aqui8')
    del B

    recommendations = slim_recommender(A, W)
    print('Passou aqui9')
    print (recommendations)
        #g = open('recomendacoes','w')
        #g.write(str(recommendations))

            #print recommendations
    precisions = compute_precision(recommendations, test_file,user_sideinformation_file,beta)
    print(precisions)
    print('Passou aqui10')
        #precisions = compute_precision_as_an_oracle(recommendations, test_file)
    print ('>>> End: %s' % datetime.datetime.now())








